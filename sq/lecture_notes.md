# Software Quality

## Lecture 1

### What is quality

- popular view is often subjective, intangible (expensive, qol, luxurius)


Quality vs functionality

- functionality = what the product does (car moves and steers)
- quality = how well it does it (moves reliable and steers accurately)


Qualities should be quantifiable and measurable (professional view)

- related to specified requirements (eg. functionality)
- professional standards (eg. reliability)
- user expectation (eg. brand)

Definition of quality

- degree a system meets
    -  specified requirements
    - meets customer or user needs or expectations

- the conformance explicit stated requirements (funct. and perf.) explicit docu, ev standards and implicit characteristics

Meaning of quality

- narrow
    - lack of bug
    Bugs:(**EXAM QUESTION**)
        - Failure
        - Error
        - Detect/Fault
- widettps://www.researchgate.net/profile/Petra_Heck/publication/254916205/figure/fig3/AS:297910928461826@1448038908332/Figure-3-ISO-9126-Internal-and-External-Quality-Characteristics-Level-5-Major-review.png
    - product properties
    - customer satisfaction
    - design properties
    - process properties (sequence of steps you took to achieve the product)

### Total quality management

Customer Focus
- studying customers wants and needs, gathering requ

Process improvements
- studying and implementing ways to continuously improve your process

Human side of quality
- company wide quality culture

Metrics, models, measurements and analyses
- ISO 9126
    - internal quality is what you can measure from the code itself (complexity, coverage)
    - external quality is what the customer sees (usability, security, safety)
    - quality in use (how it is perceived when used)
![](https://glossar.hs-augsburg.de/static/media/4/4a/ISO_9126.jpg)

Quality characteristics
- 
![](https://www.researchgate.net/profile/Petra_Heck/publication/254916205/figure/fig3/AS:297910928461826@1448038908332/Figure-3-ISO-9126-Internal-and-External-Quality-Characteristics-Level-5-Major-review.png)

learning goals and course content TODO

Assignment External 24.9.
           Internal 15.10.
