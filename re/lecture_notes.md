# Requirements Engineering



## Lecture 1 (Intro)

Lau:1

### Some Definitions

First define the problem properly. Don't rush to decision too fast.

A **Problem** is a difference between things as **desired** and things as **perceived**.

A **Requirement** is a condition or capability **needed** by a user to solve a **problem** or achieve an objective.

**Requirements Engineering** is a systematic approach to reduce the likelihood to develop the wrong solution.

### Requirement Types

#### Data requirements

- System states
- Input/Output formats

#### Functional requirements, each interface

- Record, compute, transform, transmit
- Theory: F(input, state) -> (output, state)
- Function list, pseudocode, activity diagram
- Screen prototype, support tasks xx to yy

#### Quality requirements

- Performance
- Usability
- Maintainability

#### Other deliverables

- Documentation
- Install, convert, train...

#### Managerial requirements

- Delivery time
- Legal
- Development
- Process

#### Helping the reader

- Business goals
- Definitions
- Diagrams

### Requirement Levels

| The goal-design scale                    |                   |
| ---------------------------------------- | :---------------- |
| Our pre-calculations shall hit within 5% | Goal-level req    |
| Product shall support cost recording and quotation with experience data | domain-level req  |
| product shall have recording and retrieval function for experience data | product-level req |
| system shall have screen pictures as shown in app xx | design-level req  |



## Lecture 2 (Elicitation)

Lau:8

**For the project:**

do more research. Compare to competing products, ask other customers.

### Elicitation

"You can not figure out requirements based on intuition and logic, you have to
discover them."

"Become the domain expert!"

### Why is elicitation difficult?

#### Interviews are hard

- Interviews are hard because important people don't have time and human human communication is not easy
- Stakeholders from different domains have never met and have different language

#### Symmetry of ignorance

- Customer knows problem but not technology
- Supplier knows tech but doesn't understand problem
- Tactic knowledge (things so "normal" that you don't talk about it)
- Things people tell you are from their context/pov

#### Kano Model (What stakeholders care about)

![Kano Model](https://upload.wikimedia.org/wikipedia/commons/6/68/Kano_model_showing_transition_over_time.png)

- performance needs (normal reqts. implement more, customer more satisfied)
- basic needs (often taken for granted. Nothing to win here, only loose if not implemented)
- delighters (if you have them, great, if you don't, not too bad) Over time
  they become performance and then basic needs as people get used to them.

|              |              | Customer               |                     |
| ------------ | ------------ | ---------------------- | ------------------- |
|              |              | **Expert**             | **Ignorant**        |
| **Supplier** | **Expert**   | Focus on reducing cost | Focus on consulting |
|              | **Ignorant** | Focus on specification | Focus on learning   |

### How Elicitation works

#### General rules for elicitation

- Care for the stakeholders problem
- Focus on the stakeholder, not on looking good
- Be human, admire weaknesses, show humor, become vulnerable
- Listen - eye contact, don't glaze over
- Expect changes
- Maintain a glossary - many req problems from simple misunderstandings/miscommunication

#### Tasks

- identify stakeholders
- identify relevant other sources of requirements
- collect raw requirements

##### Identify stakeholders

A stakeholder is:

- A person or role (also group)
- Affected by system under construction (or construction itself)
- Should have influence on requirements

Advice (TODO reorder properly)

- Look for persons that gain or loose from the system or the development
- Look for those that only think the above

### Elicitation Methods

#### Traditional survey

- interview
- questionnaires
- doc analysis

#### Group-based

- Brainstorming
- Focus Groups
- JAD/RAD
- Req Workshops

#### Cognitive/ introspective

 (look over shoulder while they work, for example look at logs for a web server to see how people use it)

- laddering
- card sorting
- think aloud
- protocol analysis

#### Contextual/observation

- Ethnography
- Observation
- Apprenticing
- Conversation analysis

#### Prototyping

 (you dream up a solution)

- Working prototypes
- Mashups/Mockups
- Drawings
- Diagramming

#### Model- or Spec-driven

(start and fill in the gaps)

- I*
- KAOS
- CREWS
- Use Cases

### Interviewing

#### Advice

- Take notes during interviews (looks better. share roles: one asks, one takes notes)
  - name (correct and customer terminology!)
  - relationship to system
  - representative and contact (name, phone, mail)
  - often useful (but sensitive): power and sentiment
- be prepared! (What do we need to know)
- Time of interviewee is very precious!
- Interview is official activity
  - Introduce yourself
  - Introduce interviewee(s)
  - Describe goal of interview
  - Aim for open problem description
  - Follow up on new information! Do not hesitate to ask!
- Be flexible
  - Many things are mentioned before you even ask - skip the Take notes during interviews (looks better. share roles: one asks, one takes notes)
    - name (correct and customer terminology!)
    - relationship to system
    - representative and contact (name, phone, mail)
    - often useful (but sensitive): power and sentiment
  - be prepared! (What do we need to know)
  - Time of interviewee is very precious!
  - Interview is official activity
    - Introduce yourself
    - Introduce interviewee(s)
    - Describe goal of interview
    - Aim for open problem description
    - Follow up on new information! Do not hesitate to ask!
  - Be flexible
    - Many things are mentioned before you even ask - skip the question then.
  - ask why and how
- Allow the interviewee to sketch things, use models and figures, focus
- Maintain a list of things that you know that you don't know

#### Types of questions

- Closed questions
  - Do only allow for a small set of specific answers (eg. yes/no)
- Open questions
  - Do not lea-d interviewee, give control to interviewee
- Plan ahead!
  - Start with open questions
  - paraphrase (2-steps-1-back: did I understand you correctly? Are you implying that...?)
  - At important points: close concrete questions, decide
  - At the end: discuss next steps, announce sharing of protocol
    - And deliver it in good time!

### Focus Groups

TODO explain what they are, pros and cons

### Cost/benefit analysis

![](https://www.technologyuk.net/computing/sad/images/cba.gif)

### Goal-domain tracing - critical areas

TODO find cool picture

### Elicitation techniques - early

| Technique                 | Pro                                      | con                                      |
| ------------------------- | ---------------------------------------- | ---------------------------------------- |
| Interviews                | Know the present & future ideas, Uncover conflicts/politics | Goals & critical issues are subjective   |
| Group interviews/sessions | Stimulate & complement each other, many & diverse stakeholders | Censorship & domination, Groupthinking   |
| Observation               | Actual current behaviour, processes      | Time consuming, misses exceptional/usability problems |

### Elicitation techniques - mid

| Technique      | pro                                      | con                                      |
| -------------- | ---------------------------------------- | ---------------------------------------- |
| Task demo      | Clarify how work done                    | Presence & Questions influence, Critical issues seldom captured |
| Questionnaires | Info from many (statistics, views, opinions) | Hard to construct, Interpretation        |
| Brainstorming  | Many ideas (none rejected)               | Many ideas (prioritizing needed), Involvement |

### Elicitation techniques - late

| Technique                       | pro                                      | con                                      |
| ------------------------------- | ---------------------------------------- | ---------------------------------------- |
| Use cases / Scenarios           | Concentration on specifics => accuracy   | Solution-oriented, Premature design      |
| Modeling, Data-flow Diagrams... | Communication, Organize info, Uncover missing/inconsistencies | Require tools, Time consuming, 'Cults'(???) |
| Prototyping                     | Visualization, Stimulate ideas, Usability centered | Solution-oriented, Premature design, 'Already done?' |

TODO: Steal "guideline/strategies" slides from the lecture without copyright infringement